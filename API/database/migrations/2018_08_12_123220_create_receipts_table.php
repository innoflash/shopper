<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('shopper_id')->unsigned();
            $table->integer('retailer_id')->unsigned();
            $table->string('receipt_id');
            $table->timestamps();

            $table->foreign('shopper_id')
                ->references('id')->on('shoppers');

            $table->foreign('retailer_id')
                ->references('id')->on('retailers');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts');
    }
}
