<?php

use App\ActivationCode;
use App\Shopper;
use Illuminate\Database\Seeder;

class ShopperSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Shopper::class, 100)
            ->create()
            ->each(function ($shopper) {
            $shopper->code()->save(factory(ActivationCode::class)->make());
        });
    }
}
