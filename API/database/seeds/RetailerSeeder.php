<?php

use App\Receipt;
use App\Retailer;
use Illuminate\Database\Seeder;

class RetailerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Retailer::class, 36)
            ->create()
            ->each(function ($retailer){
            $retailer->receipts()->saveMany(factory(Receipt::class, rand(10, 240))->make());
        });
    }
}
