<?php

use App\ActivationCode;
use App\Algo;
use App\Receipt;
use App\Retailer;
use App\Shopper;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});
$factory->define(Shopper::class, function (Faker $faker){
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->safeEmail,
        'phone' => $faker->phoneNumber,
        'auth_type' => 'Shopper Direct',
        'age' => rand(13, 99),
        'password' => md5('secret'),
        'unique_id' => Algo::generateCode()
    ];
});

$factory->define(ActivationCode::class, function (Faker $faker) {
    return [
        'code' => rand(11111, 999999)
    ];
});

$factory->define(Retailer::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'location' => $faker->address,
        'email' => $faker->companyEmail,
        'telephone' => $faker->phoneNumber,
        'retailer_id' => Algo::generateCode(8, 13),
        'active' => false
    ];
});

$factory->define(Receipt::class, function (Faker $faker) {
    return [
        'shopper_id' => rand(90, 100),
        'receipt_id' => rand(111111, 99999999)
    ];
});