<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {
    $api->post('/shopper-login', '\App\Http\Controllers\ShopperController@login');
    $api->post('/shopper-createaccount', '\App\Http\Controllers\ShopperController@createAccount');
    $api->post('/shopper-forgotpassword', '\App\Http\Controllers\ShopperController@forgotPassword');

    $api->group(['middleware' => 'shopper.shopper'], function ($api) {
        $api->post('/shopper-accountactivation', '\App\Http\Controllers\ShopperController@accountActivation');
        $api->post('/shopper-deleteprofile', '\App\Http\Controllers\ShopperController@deleteProfile');
        $api->post('/shopper-reactivate', '\App\Http\Controllers\ShopperController@resendActivation');
        $api->post('/shopper-editprofile', '\App\Http\Controllers\ShopperController@editProfile');
        $api->post('/shopper-uploadpic', '\App\Http\Controllers\ShopperController@uploadPicture');
        $api->post('/shopper-changepassword', '\App\Http\Controllers\ShopperController@changePassword');
        $api->post('/shopper-removeprofile', '\App\Http\Controllers\ShopperController@removeProfile');
        $api->post('/shopper-review', '\App\Http\Controllers\ShopperController@sendReview');

        $api->post('/shopper-receipts', '\App\Http\Controllers\ReceiptController@index');
    });

    $api->group(['middleware' => 'shopper.retailer'], function ($api) {
        $api->post('/retailer-sendreceipt', '\App\Http\Controllers\RetailerController@sendReceipt');
    });
});