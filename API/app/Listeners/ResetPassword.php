<?php

namespace App\Listeners;

use App\Events\ForgotPassword;
use Faker\Factory;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ResetPassword
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ForgotPassword  $event
     * @return void
     */
    public function handle(ForgotPassword $event)
    {
        $shopper = $event->shopper;
        $faker = Factory::create();
        $newPass = $faker->word;

        if (strlen($newPass) < 6) {
            $newPass .= '_shopper';
        }

        $shopper->password = md5($newPass);
        try {
            $shopper->save();
            /*$credentials = "tswelelopiet@gmail.com:dee2e502-3cbb-42e7-b73d-abbe07dd0fb6";

            $url = "https://www.zoomconnect.com/app/api/rest/v1/sms/send.json";

            $data = new stdClass();
            $data->message = 'Hello ' . $event->driver->first_name . '. The new password for you login is : ' . $newPass;
            $data->recipientNumber = $event->driver->phone;

            $data_string = json_encode($data);

            $result = file_get_contents($url, null, stream_context_create(array(
                'http' => array(
                    'method' => 'POST',
                    'header' => "Content-type: application/json\r\n" .
                        "Connection: close\r\n" .
                        "Content-length: " . strlen($data_string) . "\r\n" .
                        "Authorization: Basic " . base64_encode($credentials) . "\r\n",
                    'content' => $data_string,
                ),
            )));

            if ($result) {
                //echo 'Response: ' . $result;
            } else {
                //echo "POST failed";
            }*/
        } catch (\Exception $e) {
        }
    }
}
