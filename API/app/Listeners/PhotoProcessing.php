<?php

namespace App\Listeners;

use App\Events\PhotoUploaded;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Storage;

class PhotoProcessing
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  PhotoUploaded  $event
     * @return void
     */
    public function handle(PhotoUploaded $event)
    {
        $thumbIMG = base_path('storage/app/public/profiles/thumbs/') . $event->newName;
        $ogIMG = base_path('storage/app/public/profiles/') . $event->newName;

        $event->image->make($event->ogFile)->fit(100, 100, function ($constraint) {
            $constraint->upsize();
            $constraint->aspectRatio();
        }, 'center')->save($thumbIMG);

        Storage::disk('s3')->put($event->newName, fopen($thumbIMG, 'r+'), 'public');
        unlink($ogIMG);
    }
}
