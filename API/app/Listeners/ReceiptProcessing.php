<?php

namespace App\Listeners;

use App\Events\ReceiptSend;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ReceiptProcessing
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReceiptSend  $event
     * @return void
     */
    public function handle(ReceiptSend $event)
    {
        //
    }
}
