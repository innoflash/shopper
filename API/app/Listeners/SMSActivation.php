<?php

namespace App\Listeners;

use App\Events\ShopperRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class SMSActivation
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ShopperRegistered  $event
     * @return void
     */
    public function handle(ShopperRegistered $event)
    {
        //todo will sms activation code
    }
}
