<?php

namespace App\Events;

use App\Shopper;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class ShopperRegistered
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $shopper;
    public $preMessage;
    public function __construct(Shopper $shopper, $preMessage)
    {
        $this->shopper = $shopper;
        $this->preMessage = $preMessage;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
