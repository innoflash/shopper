<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Intervention\Image\ImageManager;

class PhotoUploaded
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $image;
    public $newName;
    public $ogFile;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ImageManager $image, $ogFile, $newName)
    {
        $this->image = $image;
        $this->newName = $newName;
        $this->ogFile = $ogFile;
    }


    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
