<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\ShopperRegistered' => [
            'App\Listeners\SMSActivation',
            'App\Listeners\EmailActivation',
        ],
        'App\Events\ForgotPassword' => [
            'App\Listeners\ResetPassword',
        ],
        'App\Events\PhotoUploaded' => [
            'App\Listeners\PhotoProcessing',
        ],
        'App\Events\ReceiptSend' => [
            'App\Listeners\ReceiptProcessing',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
