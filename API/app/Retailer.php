<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Retailer extends Model
{
    /****************** ACCESSORS ******************/

    function getNameAttribute($val)
    {
        return StringFormatter::titleText($val);
    }

    function getLocationAttribute($val)
    {
        return StringFormatter::titleText($val);
    }

    /****************** RELATIONSHIPS ******************/

    function receipts()
    {
        return $this->hasMany(Receipt::class, 'retailer_id');
    }
}
