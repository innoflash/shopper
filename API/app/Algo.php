<?php
/**
 * Created by PhpStorm.
 * User: Flash
 * Date: 8/12/2018
 * Time: 12:47 PM
 */

namespace App;


use function rand;
use function sizeof;
use function str_split;
use function strlen;

class Algo
{
    public static function generateCode($minCom = 4, $maxcom = 5){
        $theCode = '';
        $digits = '0123456789454387529431256982132434';
        $letters = 'qwertyuiopasdfghjklzxcvbnmgmnhuikowpruyrbvfqrwuncmsllaomeinc';

        $digitsArray = str_split($digits);
        $lettersArray = str_split($letters);

        $digitsLen = strlen($digits);
        $lettersLen = strlen($digits);

        $chosenNumber = rand($minCom, $maxcom);

        for ($x = 0; $x < $chosenNumber; $x++) {
            $chosenArray = rand(1, 2);
            if ($chosenArray == 1) {
                $arr = $digitsArray;
            }else{
                $arr = $lettersArray;
            }
            $theCode .= $arr[rand(0, sizeof($arr) - 1)];
        }

        $shopper = Shopper::where([
            'unique_id' => $theCode
        ])->first();
        if ($shopper == null) {
            return $theCode;
        }else{
            return self::generateCode();
        }
    }
}