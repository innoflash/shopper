<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use function strcmp;

class Shopper extends Model
{
    /********************* ACCESSORS *****************/
    function getFirstNameAttribute($val)
    {
        return StringFormatter::titleText($val);
    }

    function getLastNameAttribute($val)
    {
        return StringFormatter::titleText($val);
    }

    function getPictureAttribute($val)
    {
        //todo link to aws
        if (strcmp('shopper.png', $val) == 0) {
            //  return "https://d11haban5kb6k8.cloudfront.net/logo.png";
            return "https://s3.us-east-2.amazonaws.com/shopper.co.za/logo.png";
        } else {
            // return "https://d11haban5kb6k8.cloudfront.net/" . $val;
            return "https://s3.us-east-2.amazonaws.com/shopper.co.za/" . $val;
        }
    }


    /********************* RELATIONSHIPS *****************/

    function code()
    {
        return $this->hasOne(ActivationCode::class, 'shopper_id');
    }

    function reviews()
    {
        return $this->hasMany(Review::class, 'shopper_id');
    }

    function receipts()
    {
        return $this->hasMany(Receipt::class, 'shopper_id');
    }
}
