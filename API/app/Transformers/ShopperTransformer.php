<?php
/**
 * Created by PhpStorm.
 * User: Flash
 * Date: 8/11/2018
 * Time: 7:02 AM
 */

namespace App\Transformers;


use App\Shopper;
use League\Fractal\TransformerAbstract;

class ShopperTransformer extends TransformerAbstract
{
    function transform(Shopper $shopper)
    {
        return [
            'id' => (int) $shopper->id,
            'first_name' => $shopper->first_name,
            'last_name' => $shopper->last_name,
            'email' => $shopper->email,
            'phone' => $shopper->phone,
            'picture' => $shopper->picture,
            'age' => $shopper->age,
            'auth_side' => $shopper->auth_type,
            'user_id' => $shopper->unique_id,
        ];
    }
}