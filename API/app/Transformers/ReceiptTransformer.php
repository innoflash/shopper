<?php
/**
 * Created by PhpStorm.
 * User: Flash
 * Date: 8/12/2018
 * Time: 3:59 PM
 */

namespace App\Transformers;


use App\Receipt;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class ReceiptTransformer extends TransformerAbstract
{
    function transform(Receipt $receipt)
    {
        return [
            'id' => (int) $receipt->id,
            'retailer' => $receipt->retailer->name,
            'date' => Carbon::parse($receipt->created_at)->format('d M Y'),
            'time' => Carbon::parse($receipt->created_at)->format('H:i'),
            'amount' => 150.00
        ];
    }
}