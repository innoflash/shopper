<?php

namespace App\Http\Controllers;

use App\Receipt;
use App\Transformers\ReceiptTransformer;
use Illuminate\Http\Request;
use function strcmp;

class ReceiptController extends Controller
{
    function index(Request $request){
        if (strcmp($request->searchMode, 'month') == 0) {
            $receipts = Receipt::where('created_at', 'LIKE', $request->queryString . '%')
                ->where('shopper_id', $request->shopper_id)
                ->orderBy('id', 'DESC')
                ->paginate(10);
        }else{
            return 'by something';
        }
        return $this->response()->paginator($receipts, new ReceiptTransformer())->setStatusCode(200);
    }
}
