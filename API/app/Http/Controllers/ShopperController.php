<?php

namespace App\Http\Controllers;

use App\ActivationCode;
use App\Algo;
use App\Events\ForgotPassword;
use App\Events\PhotoUploaded;
use App\Events\ShopperRegistered;
use App\Review;
use App\Shopper;
use App\StringFormatter;
use App\Transformers\ShopperTransformer;
use Illuminate\Http\Request;
use Intervention\Image\ImageManager;
use function event;
use function strcmp;

class ShopperController extends Controller
{
    function login(Request $request)
    {
        $shopper = Shopper::where([
            //'phone' => $request->phone,
            'phone' => StringFormatter::makeNumber($request->phone),
            'password' => md5($request->password)
        ])->first();
        if ($shopper == null) {
            $response['success'] = false;
            $response['message'] = 'You have entered invalid login details!';
        } else {
            $transformer = new ShopperTransformer();
            $response['success'] = true;
            $response['user'] = $transformer->transform($shopper);
            if ($shopper->activated) {
                $response['message'] = 'Welcome back ' . $shopper->first_name;
                $response['activate'] = false;
            } else {
                $response['message'] = 'You have entered valid details but your account still needs to be activated';
                $response['activate'] = true;
            }
        }
        return $response;
    }

    function accountActivation(Request $request)
    {
        $shopper = Shopper::where([
            'unique_id' => $request->unique_id,
            'phone' => $request->phone,
          //  'phone' => StringFormatter::makeNumber($request->phone),
            'id' => $request->shopper_id
        ])->first();

        if ($shopper == null) {
            $response['success'] = false;
            $response['message'] = 'Your user details do not even exist in the system';
        } else {
            if (strcmp($shopper->code->code, $request->code) == 0) {
                $shopper->activated = true;
                $shopper->save();
                $response['success'] = true;
                $response['message'] = 'Hi ' . $shopper->first_name . ', your account has been activated, enjoy using our services';
            } else {
                $response['success'] = false;
                $response['message'] = "Your activation code is incorrect!";
            }
        }
        return $response;
    }

    function deleteProfile(Request $request)
    {
        $shopper = Shopper::findOrFail($request->id);
        if ($shopper == null) {
            $response['success'] = false;
            $response['message'] = 'Can`t find your account to cancel';
        } else {
            $shopper->delete();
            $response['success'] = true;
            $response['message'] = 'Account deleted successfully!';
        }
        return $response;
    }

    function resendActivation(Request $request)
    {
        $shopper = Shopper::findorFail($request->shopper_id);
        event(new ShopperRegistered($shopper, 'Here is the activation code you requested. '));
        if ($shopper == null) {
            $response['success'] = false;
            $response['message'] = 'User not found!';
        } else {
            $response['success'] = true;
            $response['message'] = 'We have just send you the activation code, check your SMS or email shortly!';
        }
        return $response;
    }

    function forgotPassword(Request $request)
    {
        $shopper = Shopper::where([
            'email' => $request->email,
            //'phone' => $request->phone,
            'phone' => StringFormatter::makeNumber($request->phone)
        ])->first();
        if ($shopper == NULL) {
            $response['success'] = false;
            $response['message'] = 'An account with these details was not found!';
        } else {
            event(new ForgotPassword($shopper));
            $response['success'] = true;
            $response['message'] = 'We have reset your password to our default one, we sent you the new password to login but don`t forget to change it once you are logged in';
        }
        return $response;
    }

    function editProfile(Request $request)
    {
        $shopper = Shopper::findOrFail($request->shopper_id);
        $shopper->first_name = $request->first_name;
        $shopper->last_name = $request->last_name;
        $shopper->email = $request->email;
        $shopper->age = $request->age;
        $shopper->phone = StringFormatter::makeNumber($request->newphone);

        try {
            $shopper->save();
            $transformer = new ShopperTransformer();
            $response['success'] = true;
            $response['message'] = 'You profile has been updated successfully!';
            $response['user'] = $transformer->transform($shopper);
        } catch (Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();
        }
        return $response;
    }

    function uploadPicture(Request $request, ImageManager $image)
    {
        if ($request->hasFile('file')) {
            $extension = $request->file('file')->getClientOriginalExtension();
            if ($extension == NULL) {
                $extension = 'jpg';
            }
            $newName = str_shuffle($request->id . time() . time() . time()) . '.' . $extension;
            $ogSave = base_path('storage/app/public/profiles/') . $newName;

            $image->make($request->file('file'))->save($ogSave);

            //$img_path = Storage::putFile('public', $request->file('file'));
            $shopper = Shopper::findOrFail($request->shopper_id);
            $shopper->image_url = $newName;
            try {
                $shopper->save();
                $transformer = new ShopperTransformer();
                event(new PhotoUploaded($image, $ogSave, $newName));
                $response['success'] = true;
                $response['message'] = 'Profile photo updated successfully';
                $response['user'] = $transformer->transform($shopper);
            } catch (\Exception $e) {
                $response['success'] = false;
                $response['message'] = $e->getMessage();
            }
        } else {
            $response['success'] = false;
            $response['message'] = 'You did not send any image';
        }
        return $response;
    }

    function changePassword(Request $request)
    {
        $shopper = Shopper::where([
            'id' => $request->shopper_id,
            'password' => md5($request->password)
        ])->first();

        if ($shopper == NULL) {
            $response['success'] = 0;
            $response['message'] = 'You have entered a wrong current password';
        } else {
            try {
                $shopper->password = md5($request->new_password);
                $shopper->save();
                $response['success'] = true;
                $response['message'] = 'Password changed successfully';
            } catch (Exception $e) {
                $response['success'] = 0;
                $response['message'] = $e->getMessage();
            }
        }
        return $response;
    }

    function removeProfile(Request $request)
    {
        $shopper = Shopper::where([
            'unique_id' => $request->unique_id,
            'password' => md5($request->password)
        ])->first();
        if ($shopper == null) {
            $response['success'] = false;
            $response['message'] = 'You cant delete this profile, the password you used is incorrect!';
        } else {
            $shopper->delete();
            $response['success'] = true;
            $response['message'] = 'Profile deleted successfully!';
        }
        return $response;
    }

    function createAccount(Request $request)
    {
        $code = new ActivationCode();
        $transformer = new ShopperTransformer();
        $shopper = new Shopper();
        $shopper->first_name = $request->first_name;
        $shopper->last_name = $request->last_name;
        $shopper->email = $request->email;
        $shopper->age = $request->age;
        $shopper->auth_type = $request->auth_type;
        $shopper->social_id = $request->social_id;
        $shopper->password = md5($request->password);
        $shopper->picture = 'shopper.png';
        $shopper->phone = StringFormatter::makeNumber($request->phone);
        $shopper->unique_id = Algo::generateCode();

        try {
            $shopper->save();

            $code->code = rand(11111, 999999);
            $shopper->code()->save($code);
            event(new ShopperRegistered($shopper, 'This is the activation code you need to continue with us .'));
            $response['success'] = true;
            $response['message'] = 'Hey there, we have created your account, what`s left is for you to activate it from the SMS/email we send you. Please you have until end of today to activate before we remove the account!';
            $response['user'] = $transformer->transform($shopper);
        } catch (Exception $e) {
            $response['success'] = false;
            $response['message'] = 'The phone number you used is already in use, why wont you try logging in';
        }

        return $response;
    }

    function sendReview(Request $request)
    {
        $review = new Review();
        $driver = Shopper::findOrFail($request->shopper_id);
        $review->message = $request->review;
        $review->side = $request->side;
        try {
            $driver->reviews()->save($review);
            $response['success'] = true;
            $response['message'] = 'Your review was received, thanks for getting in touch !!';
        } catch (\Exception $e) {
            $response['success'] = false;
            $response['message'] = $e->getMessage();
        }
        return $response;
    }
}
