<?php

namespace App\Http\Middleware;

use App\Shopper;
use App\StringFormatter;
use Closure;

class ShopperMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $shopper = Shopper::where([
            //'phone' => StringFormatter::makeNumber($request->phone),
            'phone' => $request->phone,
            'unique_id' => $request->unique_id
        ])->first();
        if ($shopper == null) {
            return [
                'success' => false,
                'code' => 501,
                'message' => 'You are not allowed to access this content',
                'reason' => [
                    'message' => 'You are not authenticated to be here',
                    'code' => 123
                ]
            ];
        }else{
            return $next($request);
        }
    }
}
