<?php

namespace App\Http\Middleware;

use App\Retailer;
use Closure;

class RetailerMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $retailer = Retailer::where([
            //'phone' => StringFormatter::makeNumber($request->phone),
            'telephone' => $request->telephone,
            'retailer_id' => $request->retailer_id
        ])->first();
        if ($retailer == null) {
            return [
                'success' => false,
                'code' => 501,
                'message' => 'You are not allowed to access this content',
                'reason' => [
                    'message' => 'You are not authenticated to be here',
                    'code' => 123
                ]
            ];
        }else{
            return $next($request);
        }
    }
}
