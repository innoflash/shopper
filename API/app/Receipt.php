<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    /******************** RELATIONSHIPS ********************/
    function shopper(){
        return $this->belongsTo(Shopper::class, 'shopper_id');
    }

    function retailer(){
        return $this->belongsTo(Retailer::class, 'retailer_id');
    }
}
