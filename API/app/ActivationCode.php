<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ActivationCode extends Model
{
    /***************** RELATIONSHIPS ****************/
    function shopper()
    {
        return $this->belongsTo(Shopper::class, 'shopper_id');
    }
}
