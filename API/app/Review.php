<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    /**************** ACCESSORS *******************/
    function shopper()
    {
        return $this->belongsTo(Shopper::class, 'shopper_id');
    }
}
